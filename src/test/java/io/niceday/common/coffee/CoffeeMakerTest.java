package io.niceday.common.coffee;

import com.google.common.collect.ImmutableList;
import io.niceday.common.coffee.common.maker.Maker;
import io.niceday.common.coffee.common.maker.Target;
import io.niceday.common.coffee.maker.backend.BackendMaker;
import io.niceday.common.coffee.maker.frontend.FrontendMaker;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

public class CoffeeMakerTest {

	private List<Maker>  makers  = null;
	private List<Target> targets = null;

	@Before
	public void before(){
		makers  = ImmutableList.of(new BackendMaker(), new FrontendMaker());
		targets = ImmutableList.of(
			 //Target.builder().clazz(Board.class  ).author("lucas").packages("example.comment").path("example/comment").api("/comments").build()
		);
	}

	@Ignore
	@Test
	@SneakyThrows
	public void t01_generate() {
		for(Maker maker : makers) {
			for(Target target : targets) {
				maker.generate(target);
			}
		}
	}
}