package io.niceday.common.coffee.common.exception;

import java.lang.reflect.Field;

public class TypeNotFoundException extends RuntimeException {

    public TypeNotFoundException(Field field){
        super(field.toString());
    }
}
