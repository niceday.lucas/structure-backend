package io.niceday.common.coffee.maker.frontend;

import io.niceday.common.coffee.common.exception.ColumnDefinitionNotFoundException;
import io.niceday.common.coffee.common.exception.CommentNotFoundException;
import io.niceday.common.coffee.common.exception.TypeNotFoundException;
import io.niceday.common.coffee.common.maker.Maker;
import io.niceday.common.coffee.common.maker.Target;
import io.niceday.common.engine.annotation.entity.Description;
import io.niceday.common.engine.constant.StringConstant;
import io.niceday.sample.api.entity.Sample;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.util.ObjectUtils;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.File;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Slf4j
public class FrontendMaker implements Maker {

	@Override
	@SneakyThrows
	public void generate(Target target) {
		Class  clazz      = target.getClazz();
		String path       = target.getPath();
		String action     = getAction    (target); log.info(action);
		String getter     = getGetter    (target); log.info(getter);
		String mutation   = getMutation  (target); log.info(mutation);
		String router     = getRouter    (target); log.info(router);
		String state      = getState     (target); log.info(state);
		String store      = getStore     (target); log.info(store);
		String model      = getModel     (target); log.info(model);
		String viewAdd    = getViewAdd   (target); log.info(viewAdd);
		String viewModify = getViewModify(target); log.info(viewModify);
		String viewList   = getViewList  (target); log.info(viewList);
		String viewView   = getViewView  (target); log.info(viewView);

		FileUtils.writeStringToFile(Paths.get(String.format(OUTPUT_ACTION,      path, clazz.getSimpleName())).toFile(), action,     Charsets.UTF_8);
		FileUtils.writeStringToFile(Paths.get(String.format(OUTPUT_GETTER,      path, clazz.getSimpleName())).toFile(), getter,     Charsets.UTF_8);
		FileUtils.writeStringToFile(Paths.get(String.format(OUTPUT_MUTATION,    path, clazz.getSimpleName())).toFile(), mutation,   Charsets.UTF_8);
		FileUtils.writeStringToFile(Paths.get(String.format(OUTPUT_ROUTER,      path, clazz.getSimpleName())).toFile(), router,     Charsets.UTF_8);
		FileUtils.writeStringToFile(Paths.get(String.format(OUTPUT_STATE,       path, clazz.getSimpleName())).toFile(), state,      Charsets.UTF_8);
		FileUtils.writeStringToFile(Paths.get(String.format(OUTPUT_STORE,       path, clazz.getSimpleName())).toFile(), store,      Charsets.UTF_8);
		FileUtils.writeStringToFile(Paths.get(String.format(OUTPUT_MODEL,       path, clazz.getSimpleName())).toFile(), model,      Charsets.UTF_8);
		FileUtils.writeStringToFile(Paths.get(String.format(OUTPUT_VIEW_ADD,    path, clazz.getSimpleName())).toFile(), viewAdd,    Charsets.UTF_8);
		FileUtils.writeStringToFile(Paths.get(String.format(OUTPUT_VIEW_MODIFY, path, clazz.getSimpleName())).toFile(), viewModify, Charsets.UTF_8);
		FileUtils.writeStringToFile(Paths.get(String.format(OUTPUT_VIEW_LIST,   path, clazz.getSimpleName())).toFile(), viewList,   Charsets.UTF_8);
		FileUtils.writeStringToFile(Paths.get(String.format(OUTPUT_VIEW_VIEW,   path, clazz.getSimpleName())).toFile(), viewView,   Charsets.UTF_8);
	}

	@SneakyThrows
	private String getAction(Target target){
		String source;
		source = getSource		     (INPUT_ACTION);
		source = replaceToApi        (source, target);
		source = replaceToEntity     (source, target);
		source = replaceToPackage    (source, target, StringConstant.DOUBLE_QUOTATION);
		source = replaceToPackage    (source, target, StringConstant.SLASH);
		source = uncapitalizeToEntity(source, target);
		return source;
	}

	@SneakyThrows
	private String getGetter(Target target){
		String source;
		source = getSource			 (INPUT_GETTER);
		source = replaceToEntity     (source, target);
		source = replaceToPackage    (source, target, StringConstant.SLASH);
		source = uncapitalizeToEntity(source, target);
		return source;
	}

	@SneakyThrows
	private String getMutation(Target target){
		String source;
		source = getSource			 (INPUT_MUTATION);
		source = replaceToEntity     (source, target);
		source = replaceToPackage    (source, target, StringConstant.SLASH);
		source = uncapitalizeToEntity(source, target);
		return source;
	}

	@SneakyThrows
	private String getRouter(Target target){
		String source;
		source = getSource			 (INPUT_ROUTER);
		source = replaceToApi        (source, target);
		source = replaceToEntity     (source, target);
		source = replaceToPackage    (source, target, StringConstant.DOUBLE_QUOTATION);
		source = replaceToPackage    (source, target, StringConstant.SLASH);
		source = uncapitalizeToEntity(source, target);
		return source;
	}

	@SneakyThrows
	private String getState(Target target){
		String source;
		source = getSource			 (INPUT_STATE);
		source = replaceToEntity     (source, target);
		source = replaceToPackage    (source, target, StringConstant.SLASH);
		source = uncapitalizeToEntity(source, target);
		return source;
	}

	@SneakyThrows
	private String getStore(Target target){
		String source;
		source = getSource			 (INPUT_STORE);
		source = replaceToEntity     (source, target);
		source = replaceToPackage    (source, target, StringConstant.SLASH);
		source = uncapitalizeToEntity(source, target);
		return source;
	}

	@SneakyThrows
	private String getModel(Target target){
		String source;
		source = getSource			 (INPUT_MODEL);
		source = StringUtils.replace (source, REPLACE_MODEL_REQUEST_ADD,      getModelRequestAdd     (Arrays.asList(target.getClazz().getDeclaredFields())));
		source = StringUtils.replace (source, REPLACE_MODEL_REQUEST_MODIFY,   getModelRequestModify  (Arrays.asList(target.getClazz().getDeclaredFields())));
		source = StringUtils.replace (source, REPLACE_MODEL_RESPONSE_FINDALL, getModelResponseFindAll(Arrays.asList(target.getClazz().getDeclaredFields())));
		source = StringUtils.replace (source, REPLACE_MODEL_RESPONSE_FINDONE, getModelResponseFindOne(Arrays.asList(target.getClazz().getDeclaredFields())));
		source = replaceToEntity     (source, target);
		source = replaceToPackage    (source, target, StringConstant.SLASH);
		source = uncapitalizeToEntity(source, target);
		return source;
	}

	@SneakyThrows
	private String getViewAdd(Target target){
		String source;
		source = getSource		 	 (INPUT_VIEW_ADD);
		source = StringUtils.replace (source, REPLACE_VIEW_ADD_ATTRIBUTE, getViewAddAttribute(Arrays.asList(target.getClazz().getDeclaredFields())));
		source = replaceToApi        (source, target);
		source = replaceToEntity     (source, target);
		source = replaceToPackage    (source, target, StringConstant.DOUBLE_QUOTATION);
		source = replaceToPackage    (source, target, StringConstant.SLASH);
		source = uncapitalizeToEntity(source, target);
		return source;
	}

	@SneakyThrows
	private String getViewModify(Target target){
		String source;
		source = getSource		 	 (INPUT_VIEW_MODIFY);
		source = StringUtils.replace (source, REPLACE_VIEW_MODIFY_ATTRIBUTE, getViewModifyAttribute(Arrays.asList(target.getClazz().getDeclaredFields())));
		source = replaceToApi        (source, target);
		source = replaceToEntity     (source, target);
		source = replaceToPackage    (source, target, StringConstant.DOUBLE_QUOTATION);
		source = replaceToPackage    (source, target, StringConstant.SLASH);
		source = uncapitalizeToEntity(source, target);
		return source;
	}

	@SneakyThrows
	private String getViewList(Target target){
		String source;
		source = getSource		 	 (INPUT_VIEW_LIST);
		source = StringUtils.replace (source, REPLACE_VIEW_LIST_ATTRIBUTE,   getViewListAttribute  (Arrays.asList(target.getClazz().getDeclaredFields())));
		source = StringUtils.replace (source, REPLACE_VIEW_LIST_DESCRIPTION, getViewListDescription(Arrays.asList(target.getClazz().getDeclaredFields())));
		source = replaceToApi        (source, target);
		source = replaceToEntity     (source, target);
		source = replaceToPackage    (source, target, StringConstant.DOUBLE_QUOTATION);
		source = replaceToPackage    (source, target, StringConstant.SLASH);
		source = uncapitalizeToEntity(source, target);
		return source;
	}

	@SneakyThrows
	private String getViewView(Target target){
		String source;
		source = getSource		 	 (INPUT_VIEW_VIEW);
		source = StringUtils.replace (source, REPLACE_VIEW_VIEW_ATTRIBUTE, getViewViewAttribute(Arrays.asList(target.getClazz().getDeclaredFields())));
		source = replaceToApi        (source, target);
		source = replaceToEntity     (source, target);
		source = replaceToPackage    (source, target, StringConstant.DOUBLE_QUOTATION);
		source = replaceToPackage    (source, target, StringConstant.SLASH);
		source = uncapitalizeToEntity(source, target);
		return source;
	}

	@SneakyThrows
	private String getViewAddAttribute(List<Field> fields){
		String       source = getSource(INPUT_VIEW_ADD_ATTRIBUTE);
		StringBuffer attach = new StringBuffer();
		for(Field field : fields){
			String attribute;
			if(ObjectUtils.isEmpty(field.getAnnotation(Id.class))){
				attribute = getModelAttribute    (field, source   );
				attribute = getDescriptionByField(field, attribute);
				attribute = getPrefixIfNumber    (field, attribute);
				attribute = getPrefixInputType   (field, attribute);
				attach.append(attribute);
			}
		}
		return attach.toString();
	}

	@SneakyThrows
	private String getViewModifyAttribute(List<Field> fields){
		String       source = getSource(INPUT_VIEW_MODIFY_ATTRIBUTE);
		StringBuffer attach = new StringBuffer();
		for(Field field : fields){
			String attribute;
			if(ObjectUtils.isEmpty(field.getAnnotation(Id.class))){
				attribute = getModelAttribute    (field, source   );
				attribute = getDescriptionByField(field, attribute);
				attribute = getPrefixIfNumber    (field, attribute);
				attribute = getPrefixInputType   (field, attribute);
				attach.append(attribute);
			}
		}
		return attach.toString();
	}

	@SneakyThrows
	private String getViewListAttribute(List<Field> fields){
		String       source = getSource(INPUT_VIEW_LIST_ATTRIBUTE);
		StringBuffer attach = new StringBuffer();
		for(Field field : fields){
			if(ObjectUtils.isEmpty(field.getAnnotation(Id.class))){
				String attribute = getModelAttribute(field, source);
				attach.append(attribute);
			}
		}
		return attach.toString();
	}

	@SneakyThrows
	private String getViewListDescription(List<Field> fields){
		String       source = getSource(INPUT_VIEW_LIST_DESCRIPTION);
		StringBuffer attach = new StringBuffer();
		for(Field field : fields){
			String attribute = getDescriptionByField(field, source);
			attach.append(attribute);
		}
		return attach.toString();
	}

	@SneakyThrows
	private String getViewViewAttribute(List<Field> fields){
		String       source = getSource(INPUT_VIEW_VIEW_ATTRIBUTE);
		StringBuffer attach = new StringBuffer();
		for(Field field : fields){
			String attribute;
			attribute = getModelAttribute    (field, source   );
			attribute = getDescriptionByField(field, attribute);
			attribute = getPrefixIfNumber    (field, attribute);
			attach.append(attribute);
		}
		return attach.toString();
	}

	private String getPrefixIfNumber(Field field, String target){
			 if(Long.class       == field.getType()){ return StringUtils.replace(target, REPLACE_MODEL_TYPE, ".number"); }
		else if(Integer.class    == field.getType()){ return StringUtils.replace(target, REPLACE_MODEL_TYPE, ".number"); }
		else if(BigDecimal.class == field.getType()){ return StringUtils.replace(target, REPLACE_MODEL_TYPE, ".number"); }
		return StringUtils.replace(target, REPLACE_MODEL_TYPE, StringConstant.EMPTY);
	}

	private String getPrefixInputType(Field field, String target){
		if(Boolean.class == field.getType()){ return StringUtils.replace(target, REPLACE_MODEL_INPUT_TYPE, "checkbox"); }
		return StringUtils.replace(target, REPLACE_MODEL_INPUT_TYPE, "text");
	}

	@SneakyThrows
	private String getModelRequestAdd(List<Field> fields){
		String       source = getSource(INPUT_MODEL_REQUEST_ADD);
		StringBuffer attach = new StringBuffer();
		for(Field field : fields){
			String attribute;
			if(ObjectUtils.isEmpty(field.getAnnotation(Id.class))){
				attribute = getModelAttribute    (field, source   );
				attribute = getModelAttributeType(field, attribute);
				attribute = getModelValidation   (field, attribute);
				attach.append(attribute);
			}
		}
		return attach.toString();
	}

	@SneakyThrows
	private String getModelRequestModify(List<Field> fields){
		String       source = getSource(INPUT_MODEL_REQUEST_MODIFY);
		StringBuffer attach = new StringBuffer();
		for(Field field : fields){
			String attribute;
			attribute = getModelAttribute    (field, source   );
			attribute = getModelAttributeType(field, attribute);
			attribute = getModelValidation   (field, attribute);
			attach.append(attribute);
		}
		return attach.toString();
	}

	@SneakyThrows
	private String getModelResponseFindAll(List<Field> fields){
		String       source = getSource(INPUT_MODEL_RESPONSE_FINDALL);
		StringBuffer attach = new StringBuffer();
		for(Field field : fields){
			String attribute;
			attribute = getModelAttribute    (field, source   );
			attribute = getModelAttributeType(field, attribute);
			attach.append(attribute);
		}
		return attach.toString();
	}

	@SneakyThrows
	private String getModelResponseFindOne(List<Field> fields){
		String       source = getSource(INPUT_MODEL_RESPONSE_FINDONE);
		StringBuffer attach = new StringBuffer();
		for(Field field : fields){
			String attribute;
			attribute = getModelAttribute    (field, source   );
			attribute = getModelAttributeType(field, attribute);
			attach.append(attribute);
		}
		return attach.toString();
	}

	private String getModelAttribute(Field field, String target){
		return StringUtils.replace(target, REPLACE_MODEL_ATTRIBUTE, field.getName());
	}

	private String getModelAttributeType(Field field, String target){
   		     if(String.class        == field.getType()){ return StringUtils.replace(target, REPLACE_MODEL_TYPE, "string" ); }
		else if(Long.class          == field.getType()){ return StringUtils.replace(target, REPLACE_MODEL_TYPE, "number" ); }
		else if(Integer.class       == field.getType()){ return StringUtils.replace(target, REPLACE_MODEL_TYPE, "number" ); }
		else if(BigDecimal.class    == field.getType()){ return StringUtils.replace(target, REPLACE_MODEL_TYPE, "number" ); }
		else if(LocalTime.class     == field.getType()){ return StringUtils.replace(target, REPLACE_MODEL_TYPE, "Date"   ); }
		else if(LocalDate.class     == field.getType()){ return StringUtils.replace(target, REPLACE_MODEL_TYPE, "Date"   ); }
		else if(LocalDateTime.class == field.getType()){ return StringUtils.replace(target, REPLACE_MODEL_TYPE, "Date"   ); }
		else if(Boolean.class       == field.getType()){ return StringUtils.replace(target, REPLACE_MODEL_TYPE, "boolean"); }
		throw new TypeNotFoundException(field);
	}

	@SneakyThrows
	private String getModelValidation(Field field, String target){
		Column       column      = getColumn (field);
		StringBuffer annotations = new StringBuffer();

			 if(String.class        == field.getType()){ annotations.append(String.format(" @MaxLength(%s)", column.length()));                                                        }
		else if(Long.class          == field.getType()){ annotations.append(String.format(" @Max(%s)",       StringUtils.leftPad(StringUtils.EMPTY, column.precision(), "9"))); }
		else if(Integer.class       == field.getType()){ annotations.append(String.format(" @Max(%s)",       StringUtils.leftPad(StringUtils.EMPTY, column.precision(), "9"))); }
		else if(BigDecimal.class    == field.getType()){ annotations.append(String.format(" @Max(%s)",       StringUtils.leftPad(StringUtils.EMPTY, column.precision(), "9"))); }
		else if(LocalTime.class     == field.getType()){ annotations.append(StringConstant.EMPTY);                                                                                     }
		else if(LocalDate.class     == field.getType()){ annotations.append(StringConstant.EMPTY);                                                                                     }
		else if(LocalDateTime.class == field.getType()){ annotations.append(StringConstant.EMPTY);                                                                                     }

		if(BooleanUtils.isFalse(column.nullable())) {
			if(String.class        == field.getType()){ annotations.append(" @IsString() @IsNotEmpty()"); }
			if(Long.class          == field.getType()){ annotations.append(" @IsNumber()"  ); }
			if(Integer.class       == field.getType()){ annotations.append(" @IsNumber()"  ); }
			if(BigDecimal.class    == field.getType()){ annotations.append(" @IsNumber()"  ); }
			if(Boolean.class       == field.getType()){ annotations.append(" @IsBoolean()" ); }
			if(LocalTime.class     == field.getType()){ annotations.append(" @IsNotEmpty()"); }
			if(LocalDate.class     == field.getType()){ annotations.append(" @IsNotEmpty()"); }
			if(LocalDateTime.class == field.getType()){ annotations.append(" @IsNotEmpty()"); }
		}

		return StringUtils.replace(target, REPLACE_MODEL_VALIDATION, annotations.toString());
	}

	@SneakyThrows
	private String getDescriptionByField(Field field, String target){
		return StringUtils.replace(target, REPLACE_MODEL_DESCRIPTION, getDescription(field).value());
	}

	@SneakyThrows
	private String replaceToEntity(String source, Target target){
		return StringUtils.replace(source, Sample.class.getSimpleName(), target.getClazz().getSimpleName());
	}

	@SneakyThrows
	private String uncapitalizeToEntity(String source, Target target){
		return StringUtils.replace(source, Sample.class.getSimpleName().toLowerCase(),StringUtils.uncapitalize(target.getClazz().getSimpleName()));
	}

	@SneakyThrows
	private String replaceToPackage(String source, Target target, String endsWith){
		return StringUtils.replace(source, StringUtils.join(Sample.class.getSimpleName().toLowerCase(), endsWith), StringUtils.join(target.getPath(), endsWith));
	}

	@SneakyThrows
	private String replaceToApi(String source, Target target){
		return StringUtils.replace(source, REPLACE_SOURCE_API, target.getApi());
	}

	@SneakyThrows
	private Description getDescription(Field field){
		return Optional.ofNullable(field.getAnnotation(Description.class)).orElseThrow(CommentNotFoundException::new);
	}

	@SneakyThrows
	private Column getColumn(Field field){
		return Optional.ofNullable(field.getAnnotation(Column.class)).orElseThrow(ColumnDefinitionNotFoundException::new);
	}

	@SneakyThrows
	private String getSource(String sourcePath){
		return FileUtils.readFileToString(getFile(sourcePath), Charsets.UTF_8);
	}

	@SneakyThrows
	private File getFile(String sourcePath){
		return Paths.get(sourcePath).toFile();
	}

	private String getProjectPath(){
		String projectPath = Sample.class.getResource(File.separator).getPath();
		return projectPath.substring(NumberUtils.INTEGER_ZERO, projectPath.indexOf(INPUT_DIR));
	}


	private String INPUT_DIR                      = "corpus-api"; // 프로젝트명

	private String INPUT_ACTION                   = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/action/action.template");
	private String INPUT_GETTER                   = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/getter/getter.template");
	private String INPUT_MUTATION                 = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/mutation/mutation.template");
	private String INPUT_ROUTER                   = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/router/router.template");
	private String INPUT_STATE                    = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/state/state.template");
	private String INPUT_STORE                    = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/store/store.template");
	private String INPUT_VIEW_ADD                 = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/view/view.add.template");
	private String INPUT_VIEW_MODIFY              = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/view/view.modify.template");
	private String INPUT_VIEW_LIST                = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/view/view.list.template");
	private String INPUT_VIEW_VIEW                = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/view/view.view.template");
	private String INPUT_VIEW_VIEW_ATTRIBUTE      = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/view/view.view.attribute.template");
	private String INPUT_VIEW_ADD_ATTRIBUTE       = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/view/view.add.attribute.template");
	private String INPUT_VIEW_MODIFY_ATTRIBUTE    = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/view/view.modify.attribute.template");
	private String INPUT_VIEW_LIST_ATTRIBUTE      = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/view/view.list.attribute.template");
	private String INPUT_VIEW_LIST_DESCRIPTION    = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/view/view.list.description.template");
	private String INPUT_MODEL                    = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/model/model.template");
	private String INPUT_MODEL_REQUEST_ADD        = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/model/model.request.add.template");
	private String INPUT_MODEL_REQUEST_MODIFY     = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/model/model.request.modify.template");
	private String INPUT_MODEL_RESPONSE_FINDALL   = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/model/model.response.findall.template");
	private String INPUT_MODEL_RESPONSE_FINDONE   = StringUtils.join(getProjectPath(), INPUT_DIR, "/src/main/resources/template/frontend/sample/model/model.response.findone.template");

	private String OUTPUT_MODEL                   = StringUtils.join(getProjectPath(), "output-", INPUT_DIR, "/frontend/src/%s/model/%s.ts");
	private String OUTPUT_ACTION                  = StringUtils.join(getProjectPath(), "output-", INPUT_DIR, "/frontend/src/%s/action/%sAction.ts");
	private String OUTPUT_GETTER                  = StringUtils.join(getProjectPath(), "output-", INPUT_DIR, "/frontend/src/%s/getter/%sGetter.ts");
	private String OUTPUT_MUTATION                = StringUtils.join(getProjectPath(), "output-", INPUT_DIR, "/frontend/src/%s/mutation/%sMutation.ts");
	private String OUTPUT_ROUTER                  = StringUtils.join(getProjectPath(), "output-", INPUT_DIR, "/frontend/src/%s/router/%sRouter.ts");
	private String OUTPUT_STATE                   = StringUtils.join(getProjectPath(), "output-", INPUT_DIR, "/frontend/src/%s/state/%sState.ts");
	private String OUTPUT_STORE                   = StringUtils.join(getProjectPath(), "output-", INPUT_DIR, "/frontend/src/%s/store/%sStore.ts");
	private String OUTPUT_VIEW_ADD                = StringUtils.join(getProjectPath(), "output-", INPUT_DIR, "/frontend/src/%s/view/%sAdd.vue");
	private String OUTPUT_VIEW_MODIFY             = StringUtils.join(getProjectPath(), "output-", INPUT_DIR, "/frontend/src/%s/view/%sModify.vue");
	private String OUTPUT_VIEW_LIST               = StringUtils.join(getProjectPath(), "output-", INPUT_DIR, "/frontend/src/%s/view/%sList.vue");
	private String OUTPUT_VIEW_VIEW               = StringUtils.join(getProjectPath(), "output-", INPUT_DIR, "/frontend/src/%s/view/%sView.vue");

	private String REPLACE_SOURCE_API             = "/samples";
	private String REPLACE_VIEW_ADD_ATTRIBUTE     = "#add.attribute#";
	private String REPLACE_VIEW_MODIFY_ATTRIBUTE  = "#modify.attribute#";
	private String REPLACE_VIEW_VIEW_ATTRIBUTE    = "#view.attribute#";
	private String REPLACE_VIEW_LIST_ATTRIBUTE    = "#list.attribute#";
	private String REPLACE_VIEW_LIST_DESCRIPTION  = "#list.description#";
	private String REPLACE_MODEL_REQUEST_ADD      = "#request.add#";
	private String REPLACE_MODEL_REQUEST_MODIFY   = "#request.modify#";
	private String REPLACE_MODEL_RESPONSE_FINDONE = "#response.findone#";
	private String REPLACE_MODEL_RESPONSE_FINDALL = "#response.findall#";
	private String REPLACE_MODEL_VALIDATION       = "#model.validation#";
	private String REPLACE_MODEL_DESCRIPTION      = "#model.description#";
	private String REPLACE_MODEL_ATTRIBUTE        = "#model.attribute#";
	private String REPLACE_MODEL_TYPE             = "#model.type#";
	private String REPLACE_MODEL_INPUT_TYPE       = "#model.input.type#";
}